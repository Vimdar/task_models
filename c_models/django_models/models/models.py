from __future__ import unicode_literals

from django.db import models
import django.utils.timezone as tz


class BotItem(models.Model):
    item_id = models.AutoField(primary_key=True)
    original_date = models.DateTimeField(default=tz.now)
    item_id_in_source = models.CharField(max_length=100)
    photo_url = models.CharField(max_length=255, blank=True, null=True)
    summary = models.CharField(max_length=300, blank=True, null=True)
    full_details = models.TextField(blank=True, null=True)
    crime_type = models.CharField(max_length=300, blank=True, null=True)
    crime_location = models.CharField(max_length=300, blank=True, null=True)
    crime_location = models.CharField(max_length=300, blank=True, null=True)
    suspect_name = models.CharField(max_length=300, blank=True, null=True)
    suspect_nickname = models.CharField(max_length=300, blank=True, null=True)
    number_of_people_involved = models.CharField(max_length=4, blank=True, null=True)
    cs_refference = models.CharField(max_length=100, blank=True, null=True)
    police_force = models.CharField(max_length=100, blank=True, null=True)
    suspect_sex = models.CharField(max_length=10, blank=True, null=True)
    suspect_age_range = models.CharField(max_length=30, blank=True, null=True)
    suspect_height = models.CharField(max_length=60, blank=True, null=True)
    suspect_build = models.CharField(max_length=60, blank=True, null=True)
    hair_colour = models.CharField(max_length=30, blank=True, null=True)
    hair_type = models.CharField(max_length=30, blank=True, null=True)
    hair_length = models.CharField(max_length=30, blank=True, null=True)
    facial_hair = models.CharField(max_length=30, blank=True, null=True)
    ethnic_appearance = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.suspect_name

    class Meta:
        # managed = False
        db_table = 'bot_items\".\"items'

CRAWLER MODELS
===========================

These are Django models to setup and manage psql database for a crawler_bot project.

Installation
------------

Install with easy_install or pip. The setup uses setuptools.

.. code:: sh

    pip install git+ssh:/git@gitlab.com/Vimdar/crawler.git
    easy_install git+ssh:/git@gitlab.com/Vimdar/crawler.git

Usage
-----

Include as dependency
~~~~~~~~~~~~~~~~~~~~~

To include as a dependency to a python project
append the following lines to your ``requirements.txt``.

::

    --index-url git+ssh:/git@gitlab.com/Vimdar/crawler.git
    git+ssh:/git@gitlab.com/Vimdar/crawler.git


In code
~~~~~~~

Import as standard module, include in settings, run migrations and good to go.



**WARNING: These models create and manage a certain database.
You can change them but no backwards compatibility is guaranteed

*Don't commit password in the project's repository.
Import them in `settings.py` like `from secrets import *`
where secrets.py is an untracked file.*

Development
___________

Tests
~~~~~

No tests so far #TODO

Repository
~~~~~~~~~~
----------

import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='c_models.django_models.models',
    version='0.0.0',
    namespace_packages=['c_models', 'c_models.django_models'],
    packages=[p for p in find_packages() if p.partition('.')[0] == 'c_models'],
    include_package_data=True,
    license='Mitaka',
    install_requires=[
        'django==1.11',
        'psycopg2',
        'six',
    ],
    description=('This package provides models'),
    author='Dimitar Varbenov',
    author_email='dimitarvarbenov@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
    ],
    zip_safe=True,
)
